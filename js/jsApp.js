/**
 * Created by octavioroscioli on 6/1/15.
 */

"use strict";

var jsApp = angular.module("jsApp", ["ngMaterial", "ngResource", "ui.bootstrap"]);

jsApp.factory("GithubService", function($resource) {
    return $resource("https://api.github.com/legacy/repos/search/:query",
        {query: "@query"}, {get: {method:'GET', cache: true}});
});

jsApp.controller("searchCtrl", function($scope, GithubService) {

    $scope.repos = [];
    $scope.pagination = {
        currentPage: 1,
        itemsPerPage: 20
    };

    $scope.getReposForPage = function() {
        var end = $scope.pagination.currentPage * $scope.pagination.itemsPerPage;
        var start = end - $scope.pagination.itemsPerPage;
        return $scope.repos.slice(start, end);
    };

    $scope.showPagination = function() {
        return $scope.repos.length > $scope.pagination.itemsPerPage;
    };

    $scope.search = function(query) {
        $scope.pagination.currentPage = 1;
        $scope.loading = true;
        GithubService.get({query: query}, function(res) {
            $scope.loading = false;
            $scope.repos = res.repositories;
        }, function() {
            $scope.loading = false;
            $scope.repos = [];
        });
    };
});
