/**
 * Created by octavioroscioli on 5/31/15.
 */

"use strict";

var cssApp = angular.module("cssApp", ["ngMaterial"]);

cssApp.controller("boxesCtrl", function($scope, $timeout) {
    $scope.number = 12;
    $scope.getNumber = function(num) {
        return new Array(num);
    };

    $scope.animate = function() {
        var boxes = $(".box");
        boxes.css({opacity: 0});
        boxes.animate({opacity: 1});
    };

    $timeout($scope.animate, 50);
});