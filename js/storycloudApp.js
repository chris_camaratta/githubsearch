/**
 * Created by octavioroscioli on 6/1/15.
 */

"use strict";

var storycloudApp = angular.module("storycloudApp", ["ngRoute", "jsApp", "cssApp"]);

storycloudApp.config(function($routeProvider) {

    $routeProvider
        .when("/", {
            templateUrl: "views/jsApp.html"
        })
        .when("/js", {
            templateUrl: "views/jsApp.html"
        })
        .when("/css", {
            templateUrl: "views/cssApp.html"
        })
        .otherwise({
            redirectTo: "/"
        });
});

storycloudApp.controller("SidenavCtrl", function($scope, $timeout, $mdSidenav, $location) {
    $scope.openLeftMenu = function() {
        $mdSidenav("left").toggle();
    };

    $scope.close = function() {
        $mdSidenav("left").close();

    };

    $scope.closeAndRoute = function(path) {
        $scope.close();
        $location.path("/" + path);
    };

    $scope.isCssPage = function() {
        return $location.path().indexOf("css") >= 0;
    }
});